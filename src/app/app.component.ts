import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import {Validators, FormBuilder, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { UserService } from './services/user.service';
import { AlertController } from '@ionic/angular';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  //declaro variables
  nombre: any = "nombre";
  apellido: any;
  private insertForm : FormGroup;

  constructor(
    public alertController: AlertController,
    public usr: UserService,
    private formBuilder: FormBuilder,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();

    this.insertForm = this.formBuilder.group({
      nombre: ['', Validators.required],
      apellido: ['', Validators.required],
    });

  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  adUser(){
    console.log(this.insertForm.value.nombre, this.insertForm.value.apellido);
    this.usr.addUser(this.insertForm.value.nombre, this.insertForm.value.apellido);
  }

  getApi(){
    this.usr.testGetMethod().subscribe(
      (data: any) => { 
        console.log(data); 
        this.callmePresentAlert("La api ha respondido con exito, verifique la respuesta en la consola de chrome");
      }, (error) => { 
        console.log("Error al obtener la data "+ error) 
      });

  }

  async callmePresentAlert(msj) {
    const alert = await this.alertController.create({
      backdropDismiss: false,
      header: 'INFORMACION',
      message: msj,
      buttons: [
        {
          text: 'Entendido',
          role: 'cancel',
        }
      ]
    });

    await alert.present();
  }

}
