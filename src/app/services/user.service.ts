import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  //declaro variables
  url1: string = "https://swapi.co/api/people/";
  public usuarios: Array<{ nombre: string; apellido: string; icon: string}> = [];
  
  constructor(
    private http: HttpClient,
  ) { 
    //seteo el valor inicial del array
    this.usuarios = [
      {
        nombre: 'German',
        apellido: 'Cisneros',
        icon: 'person'
      },
      {
        nombre: 'Juan',
        apellido: 'Perez',
        icon: 'person'
      },
      
    ];
    console.log(this.usuarios);
  }

  //retorna un observable con la respuesta de la api
  testGetMethod(){
    const respRecuperar = this.http.get(this.url1)
    return respRecuperar;
  }

  //añade un usuario al arreglo
  addUser(nameToAdd:string, apellidoToAdd: string){
    this.usuarios.push({
      nombre: nameToAdd,
      apellido: apellidoToAdd,
      icon: "person"
    });
  }



}
